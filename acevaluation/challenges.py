
from .utils import Cell

frameworks = ["PRG", "BOOM", "CAF"]

n_rows = 7

challenge_anchors = {
    "lephad": {
        "ggH": Cell("ggH lep-had", "A", 6),
        "Fakes": Cell("Fakes lep-had", "A", 6),
        "Data": Cell("lep-had other", "A", 6),
        "Total Background": Cell("lep-had other", "A", 16),
        "Total Signal": Cell("lep-had other", "A", 25),
        "Ztt (QCD)": Cell("lep-had other", "A", 35),
        "W": Cell("lep-had other", "A", 46),
        "Zll (QCD)": Cell("lep-had other", "A", 56),
        "Top": Cell("lep-had other", "A", 66),
        "Diboson": Cell("lep-had other", "A", 76),
    },
    "hadhad": {
        "ggH": Cell("ggH had-had", "A", 5),
        "Fakes": Cell("Fakes had-had", "A", 6),
        "Data": Cell("had-had other", "A", 6),
        "Total Background": Cell("had-had other", "A", 17),
        "Total Signal": Cell("had-had other", "A", 27),
        "Ztt (QCD)": Cell("had-had other", "A", 38),
        "Zll (QCD)": Cell("had-had other", "A", 48),
    },
    "leplep": {
        "Data": Cell("lep-lep", "A", 4),
        "ggH": Cell("lep-lep", "A", 15),
        "Ztt (QCD)": Cell("lep-lep", "A", 25),
        "Fakes": Cell("lep-lep", "A", 35),
        "Total Background": Cell("lep-lep", "A", 45),
        "Total Signal": Cell("lep-lep", "A", 55),
    },
    "zll": {
        "Data": Cell("zll_vr", "A", 4),
        "Zll (EWK)": Cell("zll_vr", "A", 14),
        "Zll (QCD)": Cell("zll_vr", "A", 23),
        # "Total Background": Cell("zll_vr", "A", 32),
    }
}
