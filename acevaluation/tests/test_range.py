
# Copyright (C) 2020 Frank Sauerburger

import unittest
from acevaluation.utils import Cell, Range

class RangeTestCase(unittest.TestCase):
    """
    Test the implementation of Range.
    """

    def test_init(self):
        """Check that the two cells are stored."""
        cell_a = Cell("this sheet", "A", 3)
        cell_b = Cell("this sheet", "D", 12)

        cell_range = Range(cell_a, cell_b)

        self.assertEqual(cell_range.cell_from, cell_a)
        self.assertEqual(cell_range.cell_to, cell_b)

    def test_different_sheets(self):
        """Check that a ValueError is raised if the cells are on two sheets"""
        cell_a = Cell("this sheet", "A", 3)
        cell_b = Cell("other sheet", "D", 12)

        self.assertRaises(ValueError, Range, cell_a, cell_b)

    def test_str(self):
        """Check taht the string representation is valid A1."""
        cell_a = Cell("this sheet", "A", 3)
        cell_b = Cell("this sheet", "D", 12)

        cell_range = Range(cell_a, cell_b)

        self.assertEqual(str(cell_range), "'this sheet'!A3:D12")
