
# Copyright (C) 2020 Frank Sauerburger

import unittest
from acevaluation.utils import Cell

class CellTestCase(unittest.TestCase):
    """
    Test the implementation of Cell.
    """

    def test_init(self):
        """Check that sheet, tcolumn and row are stored internally."""
        cell = Cell("this sheet", "B", 12)

        self.assertEqual(cell.sheet, "this sheet")
        self.assertEqual(cell.column, "B")
        self.assertEqual(cell.row, 12)

    def test_str(self):
        """Check that the string representation is valid A1."""
        cell = Cell("this sheet", "B", 12)
        self.assertEqual(str(cell), "'this sheet'!B12")

    def test_str_slash(self):
        """Check that escaping respects slashs."""
        cell = Cell("th\\is sheet", "B", 12)
        self.assertEqual(str(cell), "'th\\is sheet'!B12")

    def test_str_double_quotes(self):
        """Check that escaping respects double quotes."""
        cell = Cell("th\"is sheet", "B", 12)
        self.assertEqual(str(cell), "'th\"is sheet'!B12")

    def test_str_single_quotes(self):
        """Check that escaping respects single quotes."""
        cell = Cell("th'is sheet", "B", 12)
        self.assertEqual(str(cell), "'th''is sheet'!B12")

        cell = Cell("th''is sheet", "B", 12)
        self.assertEqual(str(cell), "'th''''is sheet'!B12")

        cell = Cell("th''is s'heet", "B", 12)
        self.assertEqual(str(cell), "'th''''is s''heet'!B12")

    def test_num_to_col(self):
        """Check taht numbers are convert to (multi) column letters."""
        self.assertEqual(Cell.num_to_col(1), 'A')
        self.assertEqual(Cell.num_to_col(10), 'J')
        self.assertEqual(Cell.num_to_col(26), 'Z')
        self.assertEqual(Cell.num_to_col(27), 'AA')
        self.assertEqual(Cell.num_to_col(26 * 2), 'AZ')
        self.assertEqual(Cell.num_to_col(26 * 2 + 1), 'BA')
        
    def test_col_to_num(self):
        """Check taht numbers are convert to (multi) column letters."""
        self.assertEqual(Cell.col_to_num('A'), 1)
        self.assertEqual(Cell.col_to_num('J'), 10)
        self.assertEqual(Cell.col_to_num('Z'), 26)
        self.assertEqual(Cell.col_to_num('AA'), 27)
        self.assertEqual(Cell.col_to_num('AZ'), 26 * 2)
        self.assertEqual(Cell.col_to_num('BA'), 26 * 2 + 1)

    def test_move(self):
        """Check that move() returns the correct location."""
        cell = Cell("sheet", "B", 12)
        cell = cell.move(2, 5)
        self.assertEqual(str(cell), "'sheet'!D17")

        cell = Cell("sheet", "B", 12)
        cell = cell.move(-1, -5)
        self.assertEqual(str(cell), "'sheet'!A7")

        cell = Cell("sheet", "B", 12)
        cell = cell.move(26, -5)
        self.assertEqual(str(cell), "'sheet'!AB7")

    def test_move_error(self):
        """Check that move() raises an exception target outside."""
        cell = Cell("sheet", "B", 12)
        self.assertRaises(ValueError, cell.move, -2, 5)
        self.assertRaises(ValueError, cell.move, 2, -12)
