
# Copyright (C) 2020 Frank Sauerburger

import unittest
from acevaluation import parse_float

class ParseFloatTestCase(unittest.TestCase):
    """
    Test the implementation of parse_float.
    """

    def test_int(self):
        """Check that integers are parsed."""
        self.assertEqual(parse_float("32"), 32)

    def test_float(self):
        """Check that floats are parsed."""
        self.assertEqual(parse_float("3.2"), 3.2)
        self.assertEqual(parse_float("-3.2"), -3.2)

    def test_comma(self):
        """Check that floats with comma are parsed."""
        self.assertEqual(parse_float("1,433.2"), 1433.2)

