
class Cell:
    """Representation of a single Cell in A1 notation."""
    
    def __init__(self, sheet, column, row):
        """Create a new cell locator in A1 notation."""
        self.sheet = sheet
        self.column = column
        self.row = row

    @staticmethod
    def col_to_num(column):
        """Convert the column letters into a numberic format."""
        if not column:
            return 0

        letter = ord(column[-1].upper()) - ord("A") + 1
        return letter + Cell.col_to_num(column[:-1]) * 26

    @staticmethod
    def num_to_col(number):
        """Convert from a numberic to column letters."""
        N = 26
        letter = chr(ord('A') + (number - 1) % N)
        if number > N:
            return Cell.num_to_col((number - 1) // N) + letter
        return letter


    def move(self, delta_column=0, delta_row=0):
        """
        Return a new cell move by the given number of rows and columns.
        
        If the computed row is zero or negative, or the computed column is
        left of A, a ValueError is raised.
        """
        row = self.row + delta_row
        column_num = Cell.col_to_num(self.column) + delta_column

        if row < 1:
            raise ValueError(f"Computed row is less than 1: {row}")
        if column_num < 1:
            raise ValueError(f"Computed column is left of A (=1): {column_num}")

        column = Cell.num_to_col(column_num)
        return Cell(self.sheet, column, row)

    def _escape_sheet_name(self):
        """Return the escapted and quoted sheet name."""
        sheet = self.sheet.replace("'", "''")
        return f"'{sheet}'"
        
    def __str__(self):
        """Return the cell locator in A1 notation for use in the API."""
        sheet = self._escape_sheet_name()
        return f"{sheet}!{self.column}{self.row}"

class Range:
    """Representation of a range in A1 notation."""

    def __init__(self, cell_from, cell_to):
        """
        Create a new range from two cells.

        A ValueError is raised if the two cells are on different sheets.
        """
        if cell_from.sheet != cell_to.sheet:
            raise ValueError("Cells most be on the same sheet.")

        self.cell_from = cell_from
        self.cell_to = cell_to

    def __str__(self):
        """Return the range in A1 notation foro use in the API."""
        sheet = self.cell_from._escape_sheet_name()

        from_column = self.cell_from.column
        from_row = self.cell_from.row

        to_column = self.cell_to.column
        to_row = self.cell_to.row

        return f"{sheet}!{from_column}{from_row}:{to_column}{to_row}"
