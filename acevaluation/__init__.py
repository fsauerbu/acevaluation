
# Copyright (C) 2020 Frank Sauerburger

"""
Module description
"""

import pickle
import os.path
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

import acevaluation.challenges as acs
from .utils import Cell, Range

__version__ = "0.1.1"  # Also change in setup.py

SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']

def read_challenge(sheet_api, sheet_id, anchor, n_rows, frameworks):
    """Retrieve the numberic results of a single challenge."""
    cell_to = anchor.move(len(frameworks) + 1, n_rows - 1)
    selection = Range(anchor, cell_to)
    selection = str(selection)

    result = sheet_api.values().get(spreadsheetId=sheet_id,
                                    range=selection).execute()

    result = result.get('values', [])
    return result

def read_challenges(sheet_api, sheet_id, challenge_anchors, n_rows,
                    frameworks):
    """Read the numeric results for all challenges."""
    results = {}

    for channel, challenge in challenge_anchors.items():
        channel_result = {}
        for challenge_name, anchor in challenge.items():
            result = read_challenge(sheet_api, sheet_id, anchor, n_rows,
                                    frameworks)

            channel_result[challenge_name] = result

        results[channel] = channel_result

    return results

def parse_float(string):
    """Parse a float from the string cell content."""
    return float(string.replace(",", ""))


def compute_max_deviations(cells, median, log):
    """Compute the maximal deviation per framework."""
    deviations = []
    for row in cells:
        yields = [parse_float(_) for _ in row[1:]]
        if median:
            reference = np.median(yields)
        else:
            reference = np.mean(yields)

        if reference == 0:
            continue

        deviation = yields / reference - 1
        deviations.append(deviation)

    deviations = abs(np.array(deviations))
    deviations = deviations.max(axis=0)

    if log:
        deviations = np.log10(deviations)

    return deviations

def compute_metric(results, frameworks, log=False, median=False):
    """Compute the largest deviation from the mean per process x channel."""

    processes = []
    channels = []
    max_deviations = []

    for channel, result in results.items():
        for challenge_name, cells in result.items():
            max_deviation = compute_max_deviations(cells, median, log)
            max_deviations.append(max_deviation)
            processes.append(challenge_name)
            channels.append(channel)

    columns = {
        "process": processes,
        "channel": channels,
    }
    for i, framework in enumerate(frameworks):
        columns[framework] = [x[i] for x in max_deviations]

    return pd.DataFrame(columns).pivot("process", "channel")

def main(args):
    """Main CLI method"""
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    # Call the Sheets API
    sheet = service.spreadsheets()  # pylint: disable=E1101

    results = read_challenges(sheet, args.sheet_id, acs.challenge_anchors,
                              acs.n_rows, acs.frameworks)

    pivot_table = compute_metric(results, acs.frameworks,
                                 log=args.log, median=args.median)

    for framework in acs.frameworks:
        print(f"Metric for {framework}:\n")
        print(pivot_table[framework], "\n\n")

    if args.plot:
        plot(args, pivot_table, acs.frameworks)

    return pivot_table

def plot(args, pivot_table, frameworks):
    """Create plots"""
    for framework in frameworks:
        plt.clf()

        if args.log:
            vlimits = dict(vmin=-5, vmax=0)
        else:
            vlimits = dict(vmin=0, vmax=0.1)

        axs = sns.heatmap(pivot_table[framework], cmap='Blues', **vlimits)
        axs.set_title(framework)

        if args.log:
            axs.figure.axes[1].set_ylabel(r"$\log_{10}$(relative deviation)")
        else:
            axs.figure.axes[1].set_ylabel("relative deviation")

        axs.figure.tight_layout()

        if args.log:
            axs.figure.savefig(f"ac_{framework}_log.pdf")
        else:
            axs.figure.savefig(f"ac_{framework}.pdf")
