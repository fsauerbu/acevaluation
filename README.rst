acevaluation
=================================

.. Short description of the project

Quickstart
==========

Install the package using pip

.. code-block:: console

   $ pip install git+https://gitlab.cern.ch/fsauerbu/acevaluation.git

.. Once deployed to pypi
   $ pip install acevaluation

Links
=====

 * `GitLab Repository <https://gitlab.cern.ch/fsauerbu/acevaluation>`_
..  * `Documentation <https://acevaluation.readthedocs.io/>`_
..  * `pyveu on PyPi <https://pypi.org/project/acevaluation>`_