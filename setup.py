"""
This script is used to install acevaluation and all its dependencies. Run

    python setup.py install
or
    python3 setup.py install

to install the package.
"""

# Copyright (C) 2020 Frank Sauerburger

from setuptools import setup

def load_long_description(filename):
    """
    Loads the given file and returns its content.
    """
    with open(filename) as readme_file:
        content = readme_file.read()
        return content

setup(name='acevaluation',
      version='0.1.1',  # Also change in module
      packages=["acevaluation", "acevaluation.tests"],
      install_requires=["google-api-python-client",
                        "google-auth-httplib2",
                        "google-auth-oauthlib",
                        "numpy",
                        "pandas",
                        "matplotlib",
                        "seaborn",
                       ],  # Also add in requirements.txt
      test_suite='acevaluation.tests',
      scripts=['bin/ac-evaluation'],
      description='',  # Short description
      long_description=load_long_description("README.rst"),
      url="https://gitlab.cern.ch/fsauerbu/acevaluation",
      author="Frank Sauerburger",
      author_email="f.sauerburger@cern.ch",
      # keywords="physics value error uncertainty unit quantity",
      classifiers=[],  # https://pypi.org/classifiers/
      license="MIT")
